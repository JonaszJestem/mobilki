package com.example.mobile

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.mobile.api.ApiInterface
import com.example.mobile.event.create.Event
import com.example.mobile.profile.User
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class Friends : AppCompatActivity() {

    private val api = ApiInterface.create()
    private var username: String = ""
    private var ID: String = ""
    private var friendBody: User? = null
    //private var currentUser: User? = null
    private var friends = arrayListOf<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends)

        val addFriendButton = findViewById<Button>(R.id.button1)

        var friendName = findViewById<EditText>(R.id.friendText)

        addFriendButton.setOnClickListener {
            val tmp = friendName.text.toString()
            addFriend(ID, tmp)
            getUser("")

            setAdapter()
        }

        username =
            if(intent.hasExtra("username")) intent.getStringExtra("username")
            else ""
        ID =
            if(intent.hasExtra("id")) intent.getStringExtra("id")
            else ""

        getUser("")
    }

    fun setAdapter() {
        var listView = findViewById<ListView>(R.id.friend_listview)
        listView.adapter = FriendsAdapter(this, friends, ID, username)
    }

    private fun getUser(x:String): Disposable? {
        return ApiInterface.create().getUserByID(ID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {
                        friends = response.friends as ArrayList<String>
                        Toast.makeText(applicationContext, friends.toString(), Toast.LENGTH_LONG).show()
                        setAdapter()
                    }
                },
                { error ->
                    run {
                        Toast.makeText(applicationContext, error.message, Toast.LENGTH_LONG).show()
                    }
                }
            )
    }

    private fun addFriend(userID: String, friendUsername: String): Disposable? {
        return ApiInterface.create().addFriend(userID, friendUsername)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {
                        Toast.makeText(applicationContext, response.message, Toast.LENGTH_LONG).show()
                    }
                },
                { error ->
                    run {
                        Toast.makeText(applicationContext, error.message, Toast.LENGTH_LONG).show()
                    }
                }
            )
    }
}
