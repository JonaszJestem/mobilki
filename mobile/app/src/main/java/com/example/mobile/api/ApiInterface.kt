package com.example.mobile.api


import com.example.mobile.API_URL
import com.example.mobile.event.create.Event
import com.example.mobile.event.preview.discussion.messages.MessageDB
import com.example.mobile.profile.User
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface ApiInterface {
    @POST("/users")
    fun createUser(@Body user: User): Observable<Response>

    @POST("/users/login")
    fun login(@Body user: User): Observable<User>

    @GET("/events/{eventId}/invites")
    fun getInvites(@Path("eventId") eventId: String): Call<JsonArray>

    @GET("/events/{eventId}/discussion")
    fun getDiscussion(@Path("eventId") eventId: String): Call<JsonArray>

    @POST("/events/discussion")
    fun addMessage(@Body message: MessageDB): Call<Response>

    @GET("/events/{eventId}")
    fun getEvent(@Path("eventId") eventId: String): Call<JsonObject>

    @POST("/events/{eventId}/invite")
    fun inviteUser(@Path("eventId") eventId: String, @Body user: JsonObject): Call<Response>

    @POST("/users/check_password")
    fun checkPassword(@Body user: User): Call<Response>

    @POST("/users/update_password")
    fun updatePassword(@Body user: User): Call<Response>

    @POST("/users/update_picture")
    fun updatePicture(@Body user: User): Call<Response>

    @POST("/users/get_user")
    fun getUser(@Body user: User): Call<User>

    @GET("/events/invited/{user}")
    fun getEvents(@Path("user") user: String): Observable<List<Event>>

    @POST("/events")
    fun addEvent(@Body event: Event): Observable<Response>

    @GET("/users/{user_name}")
    fun getUserByName(@Path("user_name") user_name: String): Observable<List<User>>

    @GET("/users/{id}/details")
    fun getUserByID(@Path("id") id: String): Observable<User>

    @POST("/users/{id}/friends/{name}")
    fun addFriend(@Path("id")id: String, @Path("name")name: String): Observable<Response>

    @DELETE("/users/{id}/friends/{name}")
    fun delFriend(@Path("id")id: String, @Path("name")name: String): Observable<Response>

    companion object {
        private const val url = API_URL

        fun create(): ApiInterface {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create()
                )
                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .baseUrl(url)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }

    data class Response(val message: String) {
        override fun toString(): String {
            return "Response(message='$message')"
        }
    }
}