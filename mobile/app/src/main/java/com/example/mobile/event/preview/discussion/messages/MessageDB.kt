package com.example.mobile.event.preview.discussion.messages

data class MessageDB (val eventId: String, val userId: String, val content: String) {
    data class Builder(var eventId: String = "", var userId: String = "", var content: String = "") {
        fun eventId(eventId: String) = apply { this.eventId = eventId }
        fun userId(userId: String) = apply { this.userId = userId }
        fun content(content: String) = apply { this.content = content }
        fun build() = MessageDB(eventId, userId, content)
    }

    override fun toString(): String {
        return "Message(eventId=$eventId, userId=$userId, content=$content)"
    }
}