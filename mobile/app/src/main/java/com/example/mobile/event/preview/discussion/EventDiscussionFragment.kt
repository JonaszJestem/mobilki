package com.example.mobile.event.preview.discussion

import android.app.Activity
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.Toast
import com.example.mobile.API_URL
import com.example.mobile.R
import com.example.mobile.api.ApiInterface
import com.example.mobile.event.preview.discussion.messages.DiscussionContent
import com.example.mobile.event.preview.discussion.messages.DiscussionContent.ITEMS
import com.example.mobile.event.preview.discussion.messages.DiscussionContent.Message
import com.example.mobile.event.preview.discussion.messages.MessageDB
import com.example.mobile.userId
import com.google.gson.*
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.fragment_event_discussion.view.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URISyntaxException

class EventDiscussionFragment : Fragment() {

    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var messageEditText: EditText
    private lateinit var recyclerView: RecyclerView
    private lateinit var username: String
    private lateinit var eventId: String
    private lateinit var serverSocket: Socket
    private lateinit var newMessage: Emitter.Listener
    private var messageBuilder = MessageDB.Builder()
    private val api = ApiInterface.create()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            username = it.getString("username")!!
            eventId = it.getString("eventId")!!
        }
        newMessage = object: Emitter.Listener {

            override fun call(vararg args: Any?) {
                val json = (args[0] as JSONArray)[0] as JSONObject
                val userId = json.get("userId") as JSONObject
                try {
                    addMessage(json.get("content").toString().removeSurrounding("\""),
                        userId.get("username").toString().removeSurrounding("\""),
                        userId.get("picture").toString(),
                        ITEMS)
                    activity!!.runOnUiThread {
                        recyclerView.scrollToPosition(DiscussionContent.ITEMS.size - 1)
                    }
                } catch (e: JsonIOException) {
                    e.printStackTrace()
                }
            }
        }
        try {
            serverSocket = IO.socket("$API_URL/events/$eventId/discussion")
            serverSocket.on("message", newMessage)
            serverSocket.connect()
        } catch(e: URISyntaxException) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val parent = inflater.inflate(R.layout.fragment_event_discussion, container, false)
        messageEditText = parent.messageEditText
        parent.messageSendButton.setOnClickListener{sendMessage()}
        recyclerView = parent.list

        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = DiscussionRecyclerViewAdapter(
                ITEMS,
                listener
            )
        }
        getMessages()
        return parent
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
        serverSocket.disconnect()
        serverSocket.off("new message", newMessage)
    }

    private fun sendMessage() {
        if(messageEditText.text.toString() != "") {
            messageBuilder.content(messageEditText.text.toString())
            messageBuilder.userId(userId)
            messageBuilder.eventId(eventId)
            val message = messageBuilder.build()
            messageEditText.setText("")
            (context as Activity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            postNewMessage(message)
        }
    }

    private fun postNewMessage(message: MessageDB) {
        val request = api.addMessage(message)
        request.enqueue(object: Callback<ApiInterface.Response> {
            override fun onResponse(call: Call<ApiInterface.Response>, response: Response<ApiInterface.Response>) = Unit
            override fun onFailure(call: Call<ApiInterface.Response>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun getMessages() {
        val messages = api.getDiscussion(eventId)
        messages.enqueue(object: Callback<JsonArray> {
            override fun onResponse(call: Call<JsonArray>, response: Response<JsonArray>) {
                val list = ArrayList<Message>()
                response.body()!!.forEach {
                    val picture = it.asJsonObject.get("picture")
                    addMessage(it.asJsonObject.get("content").toString().removeSurrounding("\""),
                        it.asJsonObject.get("userId").asJsonObject.get("username").toString().removeSurrounding("\""),
                        picture?.toString()?.removeSurrounding("\"") ?: "",
                        list)
                }
                if(list != ITEMS) {
                    (recyclerView.adapter!! as DiscussionRecyclerViewAdapter).updateData(list)
                    ITEMS = list
                }
            }

            override fun onFailure(call: Call<JsonArray>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun addMessage(content: String, username: String, pictureString: String, list: ArrayList<Message>) {
//        var picture = pictureString.substring(1, pictureString.lastIndex)
//        picture = picture.replace("\\n", "\n")
        val belongsToCurrentUser = when(this@EventDiscussionFragment.username) { username -> true; else -> false}
//        if(picture != "blank" && !belongsToCurrentUser) {
//            val byteArray = Base64.decode(picture, Base64.DEFAULT)
//            val bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
//            list.add(Message(username, content, bitmap, when(this@EventDiscussionFragment.username) { username -> true; else -> false}))
//        } else {
            list.add(Message(username, content, belongsToCurrentUser = belongsToCurrentUser))
//        }
    }


    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Message?)
    }

    companion object {
        @JvmStatic
        fun newInstance(username: String, eventId: String) =
            EventDiscussionFragment().apply {
                arguments = Bundle().apply {
                    putString("username", username)
                    putString("eventId", eventId)
                }
            }
    }

}
