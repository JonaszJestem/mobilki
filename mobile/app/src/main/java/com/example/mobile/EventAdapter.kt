package com.example.mobile

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mobile.event.create.Event
import android.util.Base64
import android.widget.*
import android.widget.TextView
import com.example.mobile.event.preview.EventPreview

class EventAdapter(private val context: Context, src: ArrayList<Event>,private val username:String) : BaseAdapter(), Filterable {

    private lateinit var filter:EventFilter
    var source:ArrayList<Event> = src

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getItem(position: Int): Any {
        return source[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return source.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ViewHolder


        if (convertView == null) {
            view = inflater.inflate(R.layout.home_page_list_item, parent, false)

            holder = ViewHolder(view)

            holder.title = view.findViewById(R.id.event_list_title) as TextView
            holder.desc = view.findViewById(R.id.event_list_desc) as TextView
            holder.date = view.findViewById(R.id.event_list_date) as TextView
            holder.image = view.findViewById(R.id.event_list_img) as ImageView

            holder.setItemClickListener(object : ItemClickListener {
                override fun onItemClick(v: View) {
                    val eventPreviewIntent = Intent(context,EventPreview::class.java)
                    eventPreviewIntent.putExtra("username", username)
                    eventPreviewIntent.putExtra("eventId", source[position]._id)
                    context.startActivity(eventPreviewIntent)
                }
            })

            view.tag = holder

        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        val title = holder.title
        val desc = holder.desc
        val date = holder.date
        val image = holder.image

        val event = getItem(position) as Event

        title.text = event.name
        desc.text = event.description
        date.text = event.date!!.substring(0,10)
        if(event.image != null) {
            val decodedString = Base64.decode(event.image, Base64.DEFAULT)
            val imageBitMap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            image.setImageBitmap(imageBitMap)
        }

        return view
    }

    override fun getFilter(): Filter?
    {

       filter = EventFilter(source,this)

        return filter
    }


    private class ViewHolder(v: View) : View.OnClickListener{


        lateinit var title: TextView
        lateinit var desc: TextView
        lateinit var image: ImageView
        lateinit var date: TextView
        lateinit var itemListener: ItemClickListener

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            this.itemListener.onItemClick(v)
        }

        fun setItemClickListener (ic: ItemClickListener)
        {
            this.itemListener = ic
        }
    }

    interface ItemClickListener
    {

        fun onItemClick(v: View)
    }



}