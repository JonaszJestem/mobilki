package com.example.mobile

import android.R
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.constraint.Constraints.TAG
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.app.TaskStackBuilder
import android.util.Log
import com.example.mobile.event.preview.EventPreview
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class Notifications : FirebaseMessagingService() {


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        Log.d(TAG, "From: ${remoteMessage?.from}")

        remoteMessage?.data?.isNotEmpty()?.let {
            val eventPreviewIntent = Intent(applicationContext, EventPreview::class.java)
            eventPreviewIntent.putExtra("username", remoteMessage.data["username"])
            eventPreviewIntent.putExtra("eventId", remoteMessage.data["eventId"])

            val resultPendingIntent: PendingIntent? = TaskStackBuilder.create(this).run {
                addNextIntentWithParentStack(eventPreviewIntent)
                getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            }

            val notification = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(remoteMessage.notification?.title)
                .setContentText(remoteMessage.data["event"])
                .setSmallIcon(R.drawable.sym_def_app_icon)
                .build()
            val manager = NotificationManagerCompat.from(applicationContext)


            manager.notify(0, notification)
        }

        remoteMessage?.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")
        }

    }


    companion object {
        private const val CHANNEL_ID = "2137"

        fun createNotificationsChannel(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val mChannel = NotificationChannel(CHANNEL_ID, "name", importance)
                mChannel.description = "Event channel"
                val notificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(mChannel)
            }

        }
    }
}