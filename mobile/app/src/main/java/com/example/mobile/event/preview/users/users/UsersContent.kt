package com.example.mobile.event.preview.users.users

import android.graphics.Bitmap
import java.util.ArrayList

object UsersContent {

    var ITEMS: ArrayList<User> = ArrayList()

    data class User(val username: String, var bitmap: Bitmap? = null) {
        override fun toString(): String = username + " " + bitmap
    }
}
