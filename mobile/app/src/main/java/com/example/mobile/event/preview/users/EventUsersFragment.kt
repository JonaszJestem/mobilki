package com.example.mobile.event.preview.users

import android.app.Service
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.util.DisplayMetrics
import android.view.*
import android.widget.Toast
import com.example.mobile.API_URL
import com.example.mobile.R
import com.example.mobile.api.ApiInterface
import com.example.mobile.event.preview.discussion.messages.DiscussionContent
import com.example.mobile.event.preview.users.users.UsersContent.ITEMS
import com.example.mobile.event.preview.users.users.UsersContent.User
import com.google.gson.JsonArray
import com.google.gson.JsonIOException
import com.google.gson.JsonObject
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.fragment_event_users.*
import kotlinx.android.synthetic.main.fragment_event_users.view.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URISyntaxException

class EventUsersFragment : DialogFragment() {

    private var listener: OnListFragmentInteractionListener? = null
    private val api = ApiInterface.create()
    private lateinit var serverSocket: Socket
    private lateinit var recyclerView: RecyclerView
    private lateinit var eventId: String
    private lateinit var newUser: Emitter.Listener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            eventId = it.getString("eventId")!!
        }
        newUser = object: Emitter.Listener {

            override fun call(vararg args: Any?) {
                val json = (args[0] as JSONArray)[0] as JSONObject
                val pictureString = json.get("picture")?.toString() ?: ""
                try {
                    addUser(json.get("username").toString().removeSurrounding("\""),
                        pictureString,
                        ITEMS
                    )
                    activity!!.runOnUiThread {
                        recyclerView.scrollToPosition(DiscussionContent.ITEMS.size - 1)
                    }
                } catch (e: JsonIOException) {
                    e.printStackTrace()
                }
            }
        }
        try {
            serverSocket = IO.socket("$API_URL/events/$eventId/invites")
            serverSocket.on("user", newUser)
            serverSocket.connect()
        } catch(e: URISyntaxException) {
            e.printStackTrace()
        }
    }

    private fun addUser(username: String, pictureString: String, list: ArrayList<User>) {
        val picture = pictureString.replace("\\n", "\n")
        if(picture != "blank") {
            val byteArray = Base64.decode(picture, Base64.DEFAULT)
            val bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
            list.add(User(username, bitmap))
        } else {
            list.add(User(username))
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        getUsers()
        val parent = inflater.inflate(R.layout.fragment_event_users, container, false)
        parent.userAddButton.setOnClickListener{inviteUser()}
        recyclerView = parent.list

        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = UsersRecyclerViewAdapter(ITEMS, listener)
        }

        return parent
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
               throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
        serverSocket.disconnect()
        serverSocket.off("new message", newUser)
    }

    override fun onResume() {
        super.onResume()
        val metrics = DisplayMetrics()
        (context!!.getSystemService(Service.WINDOW_SERVICE) as WindowManager).defaultDisplay.getMetrics(metrics)
        val window = dialog.window
        window!!.setLayout((metrics.widthPixels * 1), (metrics.heightPixels * 0.8).toInt())
    }

    private fun inviteUser() {
        val username = userEditText.text.toString()
        if(username.isNotEmpty()) {
            val jsonObject = JsonObject()
            jsonObject.addProperty("username", username)
            val request = api.inviteUser(eventId, jsonObject)
            request.enqueue(object: Callback<ApiInterface.Response> {
                override fun onResponse(call: Call<ApiInterface.Response>, response: Response<ApiInterface.Response>) {
                    Toast.makeText(context, response.body().toString().substringAfter("\'").substringBefore("\'"), Toast.LENGTH_LONG).show()
                }

                override fun onFailure(call: Call<ApiInterface.Response>, t: Throwable) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }
            })
        }
    }

    private fun getUsers() {
        val users = api.getInvites(eventId)
        users.enqueue(object: Callback<JsonArray> {
            override fun onResponse(call: Call<JsonArray>, response: Response<JsonArray>) {
                val list = ArrayList<User>()
                response.body()!!.forEach {
                    val picture = it.asJsonObject.get("picture")
                    addUser(it.asJsonObject.get("username").toString().removeSurrounding("\""),
                        picture?.toString()?.removeSurrounding("\"") ?: "",
                        list)
                }
                if(list != ITEMS) {
                    (recyclerView.adapter!! as UsersRecyclerViewAdapter).updateData(list)
                    ITEMS = list
                }
            }

            override fun onFailure(call: Call<JsonArray>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: User?)
    }

    companion object {
        @JvmStatic
        fun newInstance(eventId: String) =
            EventUsersFragment().apply {
                arguments = Bundle().apply {
                    putString("eventId", eventId)
                }
            }
    }
}
