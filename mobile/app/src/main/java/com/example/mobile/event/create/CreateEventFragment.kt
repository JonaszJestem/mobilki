package com.example.mobile.event.create

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.DialogFragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.example.mobile.api.ApiInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.new_event.*
import java.util.Calendar.*
import android.graphics.Bitmap
import android.util.Base64
import java.io.ByteArrayOutputStream


class CreateEventFragment : DialogFragment() {
    private val REQUEST_EXTERNAL_STORAGE = 1
    private val READ_PERMISSIONS = arrayOf(READ_EXTERNAL_STORAGE)

    private val GALLERY_REQUEST_CODE = 1
    private val calendar = getInstance()
    private val eventBuilder = Event.Builder()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val username = arguments?.getString("username", "undefined")
        eventBuilder.host(username.orEmpty())

        retainInstance = true
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        eventName.setText(eventBuilder.name.orEmpty())
        eventDescription.setText(eventBuilder.description.orEmpty())
        eventLocation.setText(eventBuilder.location.orEmpty())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!hasStoragePermission()) askForStoragePermission()


        eventImage.setOnClickListener { pickFromGallery() }
        createDatePicker()
        createTimePicker()
        cancel()
        createEvent()
        createTextListener(eventDescription) { description -> eventBuilder.description(description) }
        createTextListener(eventName) { name -> eventBuilder.name(name) }
        createTextListener(eventLocation) { location -> eventBuilder.location(location) }
        publicCheckbox.setOnCheckedChangeListener { _, isChecked -> eventBuilder.public(isChecked) }
    }

    fun askForStoragePermission() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            READ_PERMISSIONS,
            REQUEST_EXTERNAL_STORAGE
        )
    }

    fun hasStoragePermission(): Boolean {
        val permission = ActivityCompat.checkSelfPermission(requireContext(), READ_EXTERNAL_STORAGE);

        return permission == PackageManager.PERMISSION_GRANTED
    }

    private fun createDatePicker() {
        datePicker.setOnClickListener {
            val datePickerDialog =
                DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, month, day ->
                    eventBuilder.date("$year-${padZero(month)}-${padZero(day)}")
                }, calendar.get(YEAR), calendar.get(MONTH), calendar.get(DAY_OF_MONTH))
            datePickerDialog.show()
        }
    }

    private fun padZero(monthOfYear: Int) = monthOfYear.toString().padStart(2, '0')

    private fun createTextListener(input: EditText, builderSetter: (String) -> Event.Builder) {
        input.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(text: Editable?) {
                builderSetter(text.toString())
            }
        })
    }

    private fun createTimePicker() {
        timePicker.setOnClickListener {
            TimePickerDialog(
                context,
                { _, hour, minute -> eventBuilder.time("$hour:$minute:00.000") },
                calendar.get(HOUR_OF_DAY),
                calendar.get(MINUTE),
                false
            ).show()
        }
    }

    private fun pickFromGallery() {
        if (!hasStoragePermission()) askForStoragePermission()
        if (!hasStoragePermission()) return

        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        val mimeTypes = arrayOf("image/jpeg", "image/png")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                GALLERY_REQUEST_CODE -> {
                    val selectedImage = data?.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor = context?.contentResolver?.query(selectedImage, filePathColumn, null, null, null)
                    cursor?.moveToFirst()
                    val columnIndex = cursor?.getColumnIndex(filePathColumn[0])
                    val imgDecodableString = cursor?.getString(columnIndex!!)
                    cursor?.close()
                    var eventBitMap = BitmapFactory.decodeFile(imgDecodableString)

                    val stream = ByteArrayOutputStream()
                    eventBitMap = Bitmap.createScaledBitmap(eventBitMap, 128, 128, false);
                    eventBitMap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                    eventImageView.setImageBitmap(eventBitMap)
                    val imageBytes = stream.toByteArray()

                    val encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                    eventBuilder.image(encodedImage)
                }
            }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.setTitle("Create new event")

        return inflater.inflate(com.example.mobile.R.layout.new_event, container, false)
    }


    private fun cancel() {
        cancelEventButton.setOnClickListener {
            fragmentManager!!.beginTransaction().remove(this).commit()
        }
    }

    private fun createEvent() {
        createEventButton.setOnClickListener {
            val event = eventBuilder.build()
            addEvent(event)
        }
    }

    private fun addEvent(event: Event): Disposable? {
        return ApiInterface.create().addEvent(event)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {
                        Toast.makeText(context, response.message, Toast.LENGTH_LONG).show()
                        fragmentManager!!.beginTransaction().remove(this).commit()
                    }
                },
                { error ->
                    run {
                        Toast.makeText(context, error.message, Toast.LENGTH_LONG).show()
                    }
                }
            )
    }
}