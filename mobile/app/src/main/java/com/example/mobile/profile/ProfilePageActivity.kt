package com.example.mobile.profile

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.StrictMode
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.mobile.R
import com.example.mobile.api.ApiInterface
import kotlinx.android.synthetic.main.activity_profile_page.*
import kotlinx.android.synthetic.main.change_password.view.*
import kotlinx.android.synthetic.main.profile_picture_choice.view.*
import okhttp3.ResponseBody
import org.json.JSONObject
import java.io.IOException


class ProfilePageActivity : AppCompatActivity() {
    private var profilePictureDialog: View? = null
    private var newImage: Bitmap? = null
    private lateinit var sc: ServerConnector
    private val bo: BitmapOperator = BitmapOperator()
    private var myProfile: Boolean = false
    private var username: String = ""
    private var profileUsername: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_page)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        username =
            if(intent.hasExtra("username")) intent.getStringExtra("username")
            else ""
        profileUsername =
            if(intent.hasExtra("profile_username")) intent.getStringExtra("profile_username")
            else username
        myProfile = username == profileUsername
        sc = ServerConnector(this, profileUsername)
        loadUserInfoFromServer()
        setupObjects()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IntentCodes.PICK_PHOTO_FOR_AVATAR.code && resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(this, "No image chosen", Toast.LENGTH_LONG).show()
                return
            }
            val inputStream = contentResolver.openInputStream(data.data!!)
            val bitmap = bo.getResizedBitmap(BitmapFactory.decodeStream(inputStream), 200, 200)
            if(profilePictureDialog != null){
                profilePictureDialog!!.circleImageView.setImageBitmap(bitmap)
                newImage = bitmap
            }
        }
    }

    private fun setupObjects(){
        friendsListButton.setOnClickListener {
            friendsListRedirect()
        }
        if(myProfile) {
            changePasswordButton.setOnClickListener {
                val dialogView = layoutInflater.inflate(R.layout.change_password, null)
                AlertDialog.Builder(this).apply {
                    setTitle(R.string.change_password)
                    setView(dialogView)
                    setPositiveButton(android.R.string.ok) { dialogInterface, _ ->
                        if (dialogView.oldPasswordEditText.text.isEmpty() || dialogView.newPasswordEditText.text.isEmpty()) {
                            dialogView.errorMessageTextView.text = resources.getString(R.string.password_empty)
                            return@setPositiveButton
                        }
                        when (sc.checkPasswordCorrectness(dialogView.oldPasswordEditText.text.toString())) {
                            true -> {
                                sc.setNewPassword(dialogView.newPasswordEditText.text.toString())
                                dialogInterface.dismiss()
                            }
                            false -> {
                                dialogView.errorMessageTextView.text = resources.getString(R.string.wrong_old_password)
                                Toast.makeText(
                                    this@ProfilePageActivity,
                                    "Old password is incorrect!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }.create().show()
            }

            profilePictureImageView.setOnClickListener {
                val dialogView = layoutInflater.inflate(R.layout.profile_picture_choice, null)
                dialogView.button.setOnClickListener {
                    val intent = Intent(Intent.ACTION_GET_CONTENT)
                    intent.type = "image/*"
                    startActivityForResult(intent, IntentCodes.PICK_PHOTO_FOR_AVATAR.code)
                }
                profilePictureDialog = dialogView
                AlertDialog.Builder(this).apply{
                    setTitle(R.string.change_profile_picture)
                    setView(dialogView)
                    setPositiveButton(android.R.string.ok){_, _ ->
                        when(newImage){
                            null -> {
                                Toast.makeText(this@ProfilePageActivity, "Please choose a photo", Toast.LENGTH_SHORT).show()
                            }
                            else -> {
                                val ok = sc.setNewProfileImage(newImage!!)
                                if(ok)
                                    profilePictureImageView.setImageBitmap(newImage)
                                newImage = null
                            }
                        }
                    }
                    setOnDismissListener {
                        profilePictureDialog = null
                    }
                }.create().show()
            }
        }
        else{
            changePasswordButton.visibility = View.GONE
        }


    }

    private fun loadUserInfoFromServer(){
        val userInfo = sc.loadUserInfo() ?: return
        usernameTextView.text = userInfo.username
        emailTextView.text = userInfo.email
        if(userInfo.picture != null) {
            profilePictureImageView.setImageBitmap(bo.decodeBitmap(userInfo.picture))
        }
    }


    private fun friendsListRedirect(){
        //TODO: redirect
    }



    class ServerConnector(private val context: Context, username: String){
        private val api = ApiInterface.create()
        private val userBuilder: User.Builder = User.Builder(username = username)

        fun checkPasswordCorrectness(password: String): Boolean{
            val user = userBuilder
                .password(password)
                .build()
            val call = api.checkPassword(user)
            try {
                val response = call.execute()
                return if(response.isSuccessful){
                    when(response.body().toString()){
                        "Response(message='OK')" -> true
                        else -> false
                    }
                }
                else false
            } catch (e: IOException) {
                Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
            }
            return false
        }

        fun setNewPassword(password: String): Boolean{
            val user = userBuilder
                .password(password)
                .build()
            val call = api.updatePassword(user)
            try {
                val response = call.execute()
                return if(response.isSuccessful){
                    Toast.makeText(context, response.body().toString(), Toast.LENGTH_SHORT).show()
                    true
                } else{
                    Toast.makeText(context, getErrorMessage(response.errorBody()!!), Toast.LENGTH_SHORT).show()
                    false
                }
            }
            catch(e: IOException){
                Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
            }
            return false
        }

        fun setNewProfileImage(newImage: Bitmap): Boolean{
            val encodedBitmap = BitmapOperator().encodeBitmap(newImage)
            val user = userBuilder
                .picture(encodedBitmap)
                .build()
            val call = api.updatePicture(user)
            try {
                val response = call.execute()
                return if(response.isSuccessful){
                    Toast.makeText(context, response.body().toString(), Toast.LENGTH_SHORT).show()
                    true
                }
                else{
                    Toast.makeText(context, getErrorMessage(response.errorBody()!!), Toast.LENGTH_SHORT).show()
                    false
                }
            }
            catch(e: IOException){
                Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
            }
            return false
        }

        fun loadUserInfo(): User?{
            val user = userBuilder.build()
            val call = api.getUser(user)
            try{
                val response = call.execute()
                if(response.isSuccessful){
                    return response.body()
                }
            }
            catch (e: IOException){
                Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
            }
            return null
        }

        private fun getErrorMessage(errorBody: ResponseBody): String{
            val jsonObject = JSONObject(errorBody.string())
            return jsonObject.getString("message")
        }
    }

    private enum class IntentCodes(val code: Int){
        PICK_PHOTO_FOR_AVATAR(3113)
    }
}

