package com.example.mobile

import android.content.Intent
import android.os.Bundle
import android.support.constraint.Constraints
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.mobile.api.ApiInterface
import com.example.mobile.profile.User
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    //private val createEventFragment = CreateEventFragment()
    private val api = ApiInterface.create()
    private var currentUser: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //supportFragmentManager.beginTransaction().add(createEventFragment, "createEvent").commit()

        val buttonRegister = findViewById<Button>(R.id.buttonRegister)
        val buttonLogin = findViewById<Button>(R.id.buttonLogin)

        Notifications.createNotificationsChannel(this)

        buttonRegister.setOnClickListener {
            val intent = Intent(this, Register :: class.java)
            startActivity(intent)
        }

        buttonLogin.setOnClickListener {
            getID("")
        }
    }

    private fun login() {
        while(currentUser == null) {

        }
        Toast.makeText(applicationContext, currentUser.toString(), Toast.LENGTH_LONG).show()
    }

    private fun callLogin(observable: Observable<User>): Disposable? {
        return observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {
                        val intent = Intent(this, HomePage::class.java)
                        userId = response._id!!
                        intent.putExtra("username", response.username)
                        intent.putExtra("id", currentUser!!._id)

                        startActivity(intent,null)
                        Toast.makeText(applicationContext, "Hello ${response.username}!", Toast.LENGTH_LONG).show()
                    }
                },
                { error ->
                    run {
                        Toast.makeText(applicationContext, error.message, Toast.LENGTH_LONG).show()
                    }
                }
            )
    }

    private fun getID(x:String): Disposable? {
        return ApiInterface.create().getUserByName(editLogin.text.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {
                        currentUser = response[0]
                        FirebaseInstanceId.getInstance().instanceId
                            .addOnCompleteListener { task ->
                                if (!task.isSuccessful) {
                                    Log.w(Constraints.TAG, "getInstanceId failed", task.exception)
                                }
                                val user = User.Builder(currentUser!!._id, editLogin.text.toString(), editPassword.text.toString())
                                    .token(task.result?.token).build()

                                callLogin(api.login(user))
                            }
                    }
                },
                { error ->
                    run {
                        Toast.makeText(applicationContext, error.message, Toast.LENGTH_LONG).show()
                    }
                }
            )
    }
}
