package com.example.mobile.event.preview.discussion.messages

import android.graphics.Bitmap
import java.util.ArrayList

object DiscussionContent {

    var ITEMS: ArrayList<Message> = ArrayList()
    
    data class Message(val username: String, val content: String, var bitmap: Bitmap? = null, val belongsToCurrentUser: Boolean)
    
    data class User(val id: String, val name: String)
}
