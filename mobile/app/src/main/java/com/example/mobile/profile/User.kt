package com.example.mobile.profile

import java.util.*


data class User(
    val _id: String?,
    val username: String?,
    val password: String?,
    val email: String?,
    val picture: String?,
    val friends: List<String>?,
    val token: String?
) {
    data class Builder(
        var _id: String? = null,
        var username: String? = "",
        var password: String? = "",
        var email: String? = "",
        var picture: String? = "",
        var friends: List<String>? = ArrayList(),
        var token: String? = null
    ) {
        fun _id(_id: String) = apply { this._id = _id }
        fun username(username: String) = apply { this.username = username }
        fun password(password: String) = apply { this.password = password }
        fun email(email: String) = apply { this.email = email }
        fun picture(picture: String) = apply { this.picture = picture }
        fun token(token: String?) = apply { this.token = token }
        fun friends(friends: List<String>) = apply { this.friends = friends }

        fun build() = User(_id, username, password, email, picture, friends, token)
    }
}