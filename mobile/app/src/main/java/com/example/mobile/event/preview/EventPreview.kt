package com.example.mobile.event.preview

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Toast
import com.example.mobile.R
import com.example.mobile.api.ApiInterface
import com.example.mobile.event.create.Event
import com.example.mobile.event.preview.discussion.EventDiscussionFragment
import com.example.mobile.event.preview.discussion.messages.DiscussionContent
import com.example.mobile.event.preview.info.EventInfoFragment
import com.example.mobile.event.preview.users.EventUsersFragment
import com.example.mobile.event.preview.users.users.UsersContent
import com.example.mobile.profile.ProfilePageActivity
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_event_preview.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.Callback

class EventPreview : AppCompatActivity(), EventDiscussionFragment.OnListFragmentInteractionListener, EventUsersFragment.OnListFragmentInteractionListener {

    private lateinit var discussionFragment: EventDiscussionFragment
    private lateinit var infoFragment: EventInfoFragment
    private lateinit var event: Event
    private val api = ApiInterface.create()
    private lateinit var username: String
    private lateinit var eventId: String
    private lateinit var name: String
    private lateinit var description: String
    private lateinit var date: String
    private lateinit var location: String
    private lateinit var bitmapString: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_preview)
        eventId = intent.extras!!.getString("eventId")!!
        username = intent.extras!!.getString("username")!!
       val request = api.getEvent(eventId)

        request.enqueue(object: Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                event = Gson().fromJson(response.body(), Event::class.java)
                name = event.name!!
                description = event.description!!
                date = event.date!!
                location = event.location!!
                bitmapString = event.image!!
                initFragments()
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    fun initFragments() {
        infoFragment = EventInfoFragment.newInstance(username, eventId, name, description, date, location, bitmapString)
        discussionFragment = EventDiscussionFragment.newInstance(username, eventId)
        supportFragmentManager.beginTransaction()
            .replace(infoContainer.id, infoFragment, "infoFragment")
            .replace(discussionContainer.id, discussionFragment, "discussionFragment")
            .commit()

    }

    override fun onListFragmentInteraction(item: DiscussionContent.Message?) {
        openProfile(item!!.username)

    }

    override fun onListFragmentInteraction(item: UsersContent.User?) {
        openProfile(item!!.username)
    }

    private fun openProfile(username: String) {
        val profilePage = Intent(this, ProfilePageActivity::class.java)
        profilePage.putExtra("username", username)
        startActivity(profilePage)
    }
}
