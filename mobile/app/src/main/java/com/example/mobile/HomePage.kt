package com.example.mobile

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.*
import com.example.mobile.api.ApiInterface
import com.example.mobile.event.create.CreateEventFragment
import com.example.mobile.event.create.Event
import com.example.mobile.profile.ProfilePageActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class HomePage : AppCompatActivity() {
    private lateinit var eventAdapter: EventAdapter
    private lateinit var eventListView: ListView
    private var eventList = ArrayList<Event>()

    var username = ""
    var ID = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_page)
        this.supportActionBar?.hide()

        if (intent.hasExtra("username")) {
            username = intent.getStringExtra("username")
        }
        if (intent.hasExtra("id")) {
            ID = intent.getStringExtra("id")
        }

        eventListView = findViewById(R.id.event_list)


        getEvents("")

        eventAdapter = EventAdapter(this, eventList,username)
        eventListView.adapter = eventAdapter

        setObjectListeners()
    }

    override fun onResume()
    {
        super.onResume()

        getEvents("")
    }


    private fun setObjectListeners() {
        val createEventFragment = CreateEventFragment().apply {
            arguments = Bundle().apply { putString("username", username) }
        }

        val profileButton = this.findViewById<Button>(R.id.profile_button)
        val friendButton = this.findViewById<Button>(R.id.friend_button)
        val newEventButton = this.findViewById<Button>(R.id.new_event_button)

        val searchBar = this.findViewById<SearchView>(R.id.searchbar)
        searchBar.queryHint = "Name of Event"


        profileButton.setOnClickListener {
            val profilePage = Intent(this, ProfilePageActivity::class.java)
            profilePage.putExtra("username", username)
            startActivity(profilePage)
        }

        friendButton.setOnClickListener {
            val friendPage = Intent(this, Friends::class.java)
            friendPage.putExtra("username", username)
            friendPage.putExtra("id", ID)
            startActivity(friendPage)
        }

        newEventButton.setOnClickListener {

            supportFragmentManager.beginTransaction().add(createEventFragment, "createEvent").commit()
        }


        searchBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {


            override fun onQueryTextChange(newText: String): Boolean {

                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean
            {

                getEvents(query)
                return false
            }

        })

        searchBar.setOnCloseListener {
            getEvents("")
            false
        }

    }


    private fun getEvents(x:String): Disposable? {
        var evList = ArrayList<Event>()
        return ApiInterface.create().getEvents(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {
                        if(x =="")
                            evList = ArrayList(response)
                        else
                        {
                            val pattertToUpperCase = x.toUpperCase()
                            response.forEach {
                                val nameToUpperCase = it.name!!.toUpperCase()
                                if (nameToUpperCase.contains(pattertToUpperCase)) {
                                    evList.add(it)
                                }
                            }
                        }
                        eventListView.adapter = EventAdapter(this, evList,username)

                    }
                },
                { error ->
                    run {
                        Toast.makeText(applicationContext, error.message, Toast.LENGTH_LONG).show()
                    }
                }
            )
    }
}
