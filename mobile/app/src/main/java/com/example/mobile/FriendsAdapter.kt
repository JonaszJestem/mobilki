package com.example.mobile

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.mobile.api.ApiInterface
import com.example.mobile.event.create.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.change_password.view.*
import android.R.id.custom
import android.app.Dialog
import android.content.Intent
import android.support.v4.content.ContextCompat.getSystemService
import android.view.Window
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import io.reactivex.Observable
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject


class FriendsAdapter(context : Context, arrayNames: List<String>, ID: String, username: String) : BaseAdapter() {
    private val mContext : Context
    private val api = ApiInterface.create()

    private var names = arrayListOf<String>()
    private var ID: String = ""
    private var currentUserName: String = ""

    private var numberEvents = 0
    public var events = arrayListOf<String>()

    init {
        this.names = arrayNames as ArrayList<String>
        this.mContext = context
        this.ID = ID
        this.currentUserName = username
    }

    override fun getCount(): Int {
        return names.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return "TEST STRING"
    }

    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(mContext)
        val rowMain = layoutInflater.inflate(R.layout.friends_list, viewGroup, false)
        //val rowMain = layoutInflater.inflate(R.layout.friend_list, viewGroup, false)

        val delButton = rowMain.findViewById<Button>(R.id.delButton)
        delButton.setOnClickListener {
            delFriend(ID, names[position])
            names.remove(names[position])
            notifyDataSetChanged()
        }

        val invButton = rowMain.findViewById<Button>(R.id.buttonInvite)
        invButton.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog .setCancelable(false)
            dialog .setContentView(R.layout.popup_invite)

            val yesBtn = dialog .findViewById(R.id.yesBtn) as Button
            val noBtn = dialog .findViewById(R.id.noBtn) as Button
            val textNameEvent = dialog .findViewById(R.id.text_view) as TextView

            yesBtn.setOnClickListener {
                inviteToEvent(names[position], textNameEvent.text.toString())
                dialog .dismiss()
            }
            noBtn.setOnClickListener {
                dialog .dismiss()
            }
            dialog .show()
        }



        val nameTextView = rowMain.findViewById<TextView>(R.id.nameText)
        nameTextView.text = names[position]

        if(position%2 == 0)
            rowMain.setBackgroundColor(Color.parseColor("#f2f2f2"));


        return rowMain
    }


    private fun inviteToEvent(username: String, nameEvent: String): Disposable?  {
        return ApiInterface.create().getEvents(currentUserName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {
                        for(i in 0..(response.size-1)) {
                            Log.d("RESPONSE: ", "IM HERE " + i.toString())
                            Log.d("COMPARE: ", response[i].name + " == " + nameEvent)
                            if(response[i].name == nameEvent) {
                                Log.d("RESULT: ", "WP")
                                if(username.isNotEmpty()) {
                                    val jsonObject = JsonObject()
                                    jsonObject.addProperty("username", username)
                                    val request = api.inviteUser(response[i]._id!!, jsonObject)
                                    request.enqueue(object : retrofit2.Callback<ApiInterface.Response> {
                                        override fun onResponse(call: retrofit2.Call<ApiInterface.Response>, response: retrofit2.Response<ApiInterface.Response>) {
                                            Log.d("RESPONSE: ", response.body().toString().substringAfter("\'").substringBefore("\'"))
                                        }
                                        override fun onFailure(call: retrofit2.Call<ApiInterface.Response>, t: Throwable) {
                                            Log.d("RESPONSE: ", t.cause.toString())
                                        }
                                    })
                                } else {
                                    Log.d("RESPONSE: ", "username empty")
                                }
                                break
                            }
                        }
                        Log.d("RESPONSE ", "Nie ma takiego wydarzenia")
                    }
                },
                { error ->
                    run {
                        Log.d("ERROR: ", error.message)
                    }
                }
            )
    }

    /*private fun getEvents(x:String): Disposable? {
        var tmp: List<Event>
        return ApiInterface.create().getEvents()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {
                        tmp = response

                        numberEvents = tmp.size

                        for(i in 0..(numberEvents-1))
                            events.add(tmp[i].name.toString())

                        Toast.makeText(mContext, events.toString(), Toast.LENGTH_LONG).show()
                    }
                },
                { error ->
                    run {
                        Toast.makeText(mContext, error.message, Toast.LENGTH_LONG).show()
                    }
                }
            )
    }*/



    fun delFriend(userID: String, friendUsername: String): Disposable? {
        return ApiInterface.create().delFriend(userID, friendUsername)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {

                    }
                },
                { error ->
                    run {
                        Log.d("ERROR: ", error.toString())
                    }
                }
            )

    }

}