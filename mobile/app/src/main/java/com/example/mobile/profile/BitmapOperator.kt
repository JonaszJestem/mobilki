package com.example.mobile.profile

import android.graphics.Bitmap
import android.graphics.Matrix
import android.util.Base64
import java.io.ByteArrayOutputStream
import android.graphics.BitmapFactory



class BitmapOperator{
    fun getResizedBitmap(bitmap: Bitmap, newWidth: Int, newHeight: Int): Bitmap{
        val width = bitmap.width
        val height = bitmap.height
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeight.toFloat() / height
        val matrix = Matrix()
        matrix.postScale(scaleWidth, scaleHeight)
        val resizedBitmap = Bitmap.createBitmap(
            bitmap, 0, 0, width, height, matrix, false
        )
        bitmap.recycle()
        return resizedBitmap
    }

    fun encodeBitmap(bitmap: Bitmap): String{
        val compressionQuality = 100
        val encodedImage: String
        val byteArrayBitmapStream = ByteArrayOutputStream()
        bitmap.compress(
            Bitmap.CompressFormat.PNG, compressionQuality,
            byteArrayBitmapStream
        )
        val b = byteArrayBitmapStream.toByteArray()
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT)
        return encodedImage
    }

    fun decodeBitmap(code: String): Bitmap{
        val decodedString = Base64.decode(code, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    }
}