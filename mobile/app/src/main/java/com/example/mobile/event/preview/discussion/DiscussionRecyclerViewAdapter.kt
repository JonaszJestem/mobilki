package com.example.mobile.event.preview.discussion

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.mobile.R
import com.example.mobile.event.preview.discussion.EventDiscussionFragment.OnListFragmentInteractionListener
import com.example.mobile.event.preview.discussion.messages.DiscussionContent.ITEMS
import com.example.mobile.event.preview.discussion.messages.DiscussionContent.Message
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.others_message.view.*

class DiscussionRecyclerViewAdapter(
    private var mValues: ArrayList<Message>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<DiscussionRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Message
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when(ITEMS[position].belongsToCurrentUser) {
            true -> 1
            false -> 0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when(viewType) {
            1 -> ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_message, parent, false), true)
            else -> ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.others_message, parent, false), false)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val message = mValues[position]
        holder.messageTextView.text = message.content
        if(!message.belongsToCurrentUser) {
            holder.messageUserView!!.text = message.username
            if(message.bitmap != null) {
                holder.messageAvatarView!!.setImageBitmap(message.bitmap)
            }
        }

        with(holder.mView) {
            tag = message
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View, belongsToCurrentUser: Boolean) : RecyclerView.ViewHolder(mView) {
        var messageTextView: TextView = mView.messageBody
        var messageAvatarView: CircleImageView? = null
        var messageUserView: TextView? = null

        init {
            if(!belongsToCurrentUser) {
                messageAvatarView = mView.messageAvatar
                messageUserView = mView.messageName
            }
        }
    }

    fun updateData(data: ArrayList<Message>) {
        mValues.clear()
        mValues = data
        notifyDataSetChanged()
    }
}
