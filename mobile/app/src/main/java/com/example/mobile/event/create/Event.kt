package com.example.mobile.event.create

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

data class Event(
    val _id: String? ,
    val name: String?,
    val description: String?,
    val date: String?,
    val location: String?,
    val host: String?,
    val image: String?,
    val public: Boolean?
) {
    data class Builder(
        var _id: String? = null,
        var name: String? = "",
        var description: String? = "",
        var date: String? = "",
        var time: String? = "",
        var location: String? = "",
        var host: String? = "",
        var image: String? = "",
        var public: Boolean = true
    ) {
        fun _id(_id: String) = apply { this._id = _id }
        fun name(name: String) = apply { this.name = name }
        fun description(description: String) = apply { this.description = description }
        fun date(date: String) = apply { this.date = date }
        fun time(time: String) = apply { this.time = time }
        fun location(location: String) = apply { this.location = location }
        fun host(host: String) = apply { this.host = host }
        fun image(image: String) = apply { this.image = image }
        fun public(public: Boolean) = apply { this.public = public }
        fun build() = Event(_id, name, description, parseDate(), location, host, image, public)

        private fun parseDate(): String? {
            return try {
                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
                return String.format("%tQ", simpleDateFormat.parse("$date $time"))
            } catch (e: ParseException) {
                null
            }
        }

    }

    override fun toString(): String {
        return "Event(name=$name, description=$description, date=$date, location=$location)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Event

        if (_id != other._id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (date != other.date) return false
        if (location != other.location) return false
        if (host != other.host) return false

        return true
    }

    override fun hashCode(): Int {
        var result = _id?.hashCode() ?:0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (date?.hashCode() ?: 0)
        result = 31 * result + (location?.hashCode() ?: 0)
        result = 31 * result + (host?.hashCode() ?: 0)
        return result
    }


}