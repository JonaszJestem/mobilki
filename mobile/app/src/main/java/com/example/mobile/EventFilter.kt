package com.example.mobile

import android.widget.Filter
import com.example.mobile.event.create.Event


class EventFilter(private val eventList:ArrayList<Event>, private val adapter:EventAdapter) : Filter()
{


    override fun performFiltering(constraint: CharSequence?): FilterResults {
        val results = FilterResults()

        if(constraint != null && constraint.isNotEmpty())
        {
            val cons = constraint.toString().toUpperCase()

            val filteredEvents = ArrayList<Event>()

            eventList.forEach {
                if(it.name?.toUpperCase()?.contains(cons)!!)
                {
                    filteredEvents.add(it)
                }
            }
            results.count = filteredEvents.size
            results.values = filteredEvents
        }
        else
        {
            results.count = eventList.size
            results.values = eventList
        }
        return results
    }

    override fun publishResults(constraint: CharSequence, results: FilterResults) {

            adapter.source = results.values as ArrayList<Event>
            adapter.notifyDataSetChanged()

    }
}