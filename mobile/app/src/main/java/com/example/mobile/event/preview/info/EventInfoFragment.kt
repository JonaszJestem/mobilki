package com.example.mobile.event.preview.info

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.mobile.R
import com.example.mobile.api.ApiInterface
import com.example.mobile.event.preview.users.EventUsersFragment
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.fragment_event_info.view.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class EventInfoFragment : Fragment() {
    private val api = ApiInterface.create()
    private lateinit var username: String
    private lateinit var eventId: String
    private lateinit var title: String
    private lateinit var description: String
    private lateinit var bitmap: Bitmap
    private lateinit var dateDay: String
    private lateinit var dateTime: String
    private lateinit var location: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            eventId = it.getString("eventId")!!
            username = it.getString("username")!!
            title = it.getString("title")!!
            description = it.getString("description")!!
            val date = it.getString("date")!!
            dateDay = date.substringBefore("T")
            dateTime = (date.substringAfter("T")).substringBeforeLast(":")
            val bitmapString = it.getString("bitmapString")
            val byteArray = Base64.decode(bitmapString, Base64.DEFAULT)
            location = it.getString("location")!!
            if (byteArray != null) {
                bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_event_info, container, false)
        view.eventInfoTitle.text = title
        view.eventInfoDescription.text = description
        view.eventInfoDate.text = dateDay
        view.eventInfoTime.text = dateTime
        view.eventInfoLocation.text = location
        view.eventInfoPhoto.setImageBitmap(bitmap)
        view.participantsButton.setOnClickListener { participantsButton() }
        view.interestedButton.setOnClickListener { interestedButton() }
        return view
    }

    private fun participantsButton() {
        val usersDialog = EventUsersFragment.newInstance(eventId)
        usersDialog.show(childFragmentManager, "users_list")
    }

    private fun interestedButton() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        val request = api.inviteUser(eventId, jsonObject)
        request.enqueue(object : Callback<ApiInterface.Response> {
            override fun onResponse(call: Call<ApiInterface.Response>, response: Response<ApiInterface.Response>) {
                Toast.makeText(
                    context,
                    response.body().toString().substringAfter("\'").substringBefore("\'"),
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onFailure(call: Call<ApiInterface.Response>, t: Throwable) {
                Toast.makeText(context, t.cause.toString(), Toast.LENGTH_LONG).show()
            }
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(
            username: String,
            eventId: String,
            title: String,
            description: String,
            date: String,
            location: String,
            bitmapString: String
        ) =
            EventInfoFragment().apply {
                arguments = Bundle().apply {
                    putString("eventId", eventId)
                    putString("username", username)
                    putString("title", title)
                    putString("description", description)
                    putString("date", date)
                    putString("location", location)
                    putString("bitmapString", bitmapString)
                }
            }
    }
}
