package com.example.mobile.event.preview.users

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.mobile.R
import com.example.mobile.event.preview.users.EventUsersFragment.OnListFragmentInteractionListener
import com.example.mobile.event.preview.users.users.UsersContent.User
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_user.view.*


class UsersRecyclerViewAdapter(
    private var mValues: ArrayList<User>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<UsersRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as User
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = mValues[position]
        val item = mValues[position]
        holder.userName.text = item.username
        if(user.bitmap != null) {
            holder.userAvatar.setImageBitmap(user.bitmap)
        }

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val userName: TextView = mView.userName
        val userAvatar: CircleImageView = mView.userAvatar

        override fun toString(): String {
            return super.toString() + " '"  + "'"
        }
    }

    fun updateData(data: ArrayList<User>) {
        mValues.clear()
        mValues = data
        notifyDataSetChanged()
    }
}
