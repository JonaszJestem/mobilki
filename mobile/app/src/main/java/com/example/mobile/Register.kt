package com.example.mobile

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.mobile.api.ApiInterface
import com.example.mobile.profile.BitmapOperator
import com.example.mobile.profile.User
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_register.*


class Register : AppCompatActivity() {
    private val api = ApiInterface.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        button.setOnClickListener {
            val user = User.Builder()
                .email(editMail.text.toString())
                .username(editLog.text.toString())
                .password(editPass.text.toString())
                .picture(BitmapOperator().encodeBitmap(BitmapFactory.decodeResource(resources, R.drawable.default_profile_picture)))
                .build()
            callRegister(api.createUser(user))
        }
    }


    private fun callRegister(observable: Observable<ApiInterface.Response>): Disposable? {
        return observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    run {
                        startActivity(intent, null)
                        Toast.makeText(applicationContext, response.toString(), Toast.LENGTH_LONG).show()
                        startActivity(Intent(this, MainActivity::class.java))
                    }
                },
                { error ->
                    run {
                        Toast.makeText(applicationContext, error.message, Toast.LENGTH_LONG).show()
                    }
                }
            )
    }
}
