const express = require('express');
const router = express.Router();
const Message = require('../database/models/Message');
const sendError = require('./response');

router.route('/events/:event_id/discussion').get(async function (req, res) {
    try {
        const messages = await Message.find({eventId: req.params.event_id}, '-_id content').populate('userId', '-_id username picture');
        res.status(200);
        res.json(messages);

    } catch (error) {
        sendError(res, error);
    }
});

router.route('/events/discussion').post(async function (req, res) {
    try {
        var message = new Message(req.body);
	const eventId = message.eventId;
        message = await message.save();
	message = await Message.find({_id: message._id}, '-_id content').populate('userId', '-_id username picture');
        const io = req.app.get('io');
        io.of('/events/'+eventId+'/discussion').emit('message', message);
        res.status(200);
        res.json({message: "Message added"});
    } catch (error) {
        sendError(res, error);
    }
});

module.exports = router;
