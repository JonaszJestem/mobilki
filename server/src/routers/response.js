function sendError(res, error) {
    console.log(error);
    res.status(400);
    res.json(error);
};

module.exports = sendError