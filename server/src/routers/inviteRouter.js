const express = require('express');
const router = express.Router();
const Event = require('../database/models/Event');
const User = require('../database/models/User');
const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
const ObjectId = mongoose.Types.ObjectId;
const admin = require('firebase-admin');

// const serviceAccount = require("../serviceKey.json");

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: 'https://mobilki-883ba.firebaseio.com'
});

router.route('/events/:event_id/invite').post(async function (req, res) {
    try {
        const id = ObjectId(req.params.event_id);
        var username = req.body.username;
        const event = await Event.find({$and: [{participants: username}, {_id: id}]});
        const user = await User.find({username: username});


        if (!user.length) {
            res.status(200);
            res.json({message: "User not found"});
        } else if (!event.length) {
            const update = await Event.findByIdAndUpdate(id, {$push: {participants: username}});
            notifyUser(username, update);
            const user = await User.find({username: username}, '-_id username picture');
            const io = req.app.get('io');
            io.of('/events/' + req.params.event_id + '/invites').emit('user', user);
            res.status(200);
            res.json({message: "User invited"});
        } else {
            res.status(200);
            res.json({message: "User already invited"});
        }
    } catch (error) {
        sendError(res, error);
    }
});

router.route('/events/:event_id/invites/').get(async function (req, res) {
    try {
        const id = ObjectId(req.params.event_id);
        var participants = await Event.find(id, '-_id participants');
        participants = participants.map(function (doc) {
            return doc.participants;
        })[0];
        const users = await User.find({username: {$in: participants}}, '-_id username picture');

        res.status(200);
        res.json(users);
    } catch (error) {
        sendError(res, error);
    }
});

async function notifyUser(username, event) {
    const user = await User.findOne({username: username});

    console.log(user);
    console.log(event);
    const payload = {
        notification: {
            title: "You are invited to an event!"
        },
        data: {
            event: event.name,
            eventId: event._id.toString(),
            username: username
        }
    };
    try {
        const response = await admin.messaging().sendToDevice(user.token, payload);
        console.log('Successfully sent message:', response);
    } catch (error) {
        console.log('Error sending message:', error);
    }
}

module.exports = router;
