const express = require('express');
const router = express.Router();
const Event = require('../database/models/Event');
const sendError = require('./response');


router.route('/').get(function (req, res) {
    res.status(200);
    res.json({message: "OK"});
});

router.route('/events/').post(async function (req, res) {
    try {
        const event = new Event({...req.body, participants: [req.body.host]});
        await event.save();

        res.status(201);
        res.json({message: "Event created"});
    } catch (error) {
        console.log(error);
        sendError(res, error)
    }
});

router.route('/events/invited/:user').get(async function (req, res) {
    try {
        const user = req.params.user;
        const events = await Event.find().or([{participants: user}, {public: true}]);

        res.status(200);
        res.json(events);
    } catch (error) {
        console.log(error);
        sendError(res, error);
    }
});

router.route('/events/:event_id/').get(async function (req, res) {
    try {
        const event = await Event.findOne({_id: req.params.event_id});

        res.status(200);
        res.json(event);
    } catch (error) {
        console.log(error);
        sendError(res, error);
    }
});

module.exports = router;
