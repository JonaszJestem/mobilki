const express = require('express');
const router = express.Router();
const User = require('../database/models/User');
const sendError = require('./response');


router.route('/users/').post(async function (req, res) {
    try {
        const user = new User(req.body);
        await user.save();

        res.status(201);
        res.json({message: "User created"});
    } catch (error) {
        console.log(error);
        sendError(res, error)
    }
});

router.route('/users/:id/details').get(async function (req, res) {
    try {
        const userId = req.params.id;
        const user = await User.findById(userId);

        res.status(200);
        res.json(user);
    } catch (error) {
        console.log(error);
        sendError(res, error)
    }
});


router.route('/users/:id/friends/:name').post(async function (req, res) {
    try {
        const friend = req.params.name;
        const userId = req.params.id;

        await User.findOneAndUpdate({_id: userId}, {$push: {friends: friend}});
        const updatedUser = await User.findById(userId);

        res.status(200);
        res.json(updatedUser);
    } catch (error) {
        console.log(error);
        sendError(res, error)
    }
});

router.route('/users/:id/friends/:name').delete(async function (req, res) {
    try {
        const friend = req.params.name;
        const userId = req.params.id;

        await User.findOneAndUpdate({_id: userId}, {$pull: {friends: friend}});
        const updatedUser = await User.findById(userId);

        res.status(200);
        res.json(updatedUser);
    } catch (error) {
        console.log(error);
        sendError(res, error)
    }
});


router.route('/users/:name').get(async function (req, res) {
    try {
        const username = new RegExp(req.params.name);
        const users = await User.find({username: username});

        res.status(200);
        res.json(users);
    } catch (error) {
        console.log(error);
        sendError(res, error)
    }
});


router.route('/users/login').post(async function (req, res) {
    try {
        const {username, password, token} = req.body;

        const user = await User.findOneAndUpdate({username, password}, {token: token});

        if (user) {
            res.status(200);
            res.json(user);
        } else {
            res.status(404);
            res.json({message: "User not found"})
        }
    } catch (error) {
        console.log(error);
        sendError(res, error);
    }
});

router.route('/users/check_password').post(async function (req, res) {
    try {
        const {username, password} = req.body;
        const user = await User.findOne({username, password});
        if (user) {
            res.status(200);
            res.json({message: "OK"});
        } else {
            res.status(404);
            res.json({message: "Wrong password"});
        }
    } catch (error) {
        sendError(res, error);
    }
});

router.route('/users/update_password').post(async function (req, res) {
    try {
        var query = {"username": req.body.username}
        var data = {$set: {"password": req.body.password}}
        User.updateOne(
            query,
            data,
            (err, collection) => {
                if (err) throw err;
                console.log("Password updated successfully");
                console.log(collection);
            }
        );
        res.status(202);
        res.json({message: "Password changed"});
    } catch (error) {
        sendError(res, error);
    }
});

router.route('/users/update_picture').post(async function (req, res) {
    try {
        await User.updateOne(
            {"username": req.body.username},
            {$set: {"picture": req.body.picture}},
            (err, collection) => {
                if (err) throw err;
                console.log("Picture updated successfully");
                console.log(collection);
            }
        );
        res.status(202);
        res.json({message: "Picture changed"});
    } catch (error) {
        sendError(res, error);
    }
});

router.route('/users/get_user').post(async function (req, res) {
    try {
        const {username} = req.body;
        const user = await User.findOne({username});
        if (user) {
            res.status(200);
            res.json(user);
        } else {
            res.status(404);
            res.json({message: "User not found"})
        }
    } catch (error) {
        sendError(res, error);
    }
});

module.exports = router;
