module.exports = {
    hostname: "mongo",
    port: 27017,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
};
