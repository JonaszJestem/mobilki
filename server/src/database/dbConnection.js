const mongoose = require('mongoose');

async function connect(dbUrl, config) {
    await mongoose.connect(dbUrl, {
        user: config.username,
        pass: config.password,
        dbName: config.database,
        useNewUrlParser: true,
        autoReconnect: true,
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000,
        poolSize: 10
    });
}

const setUpDbConnection = async (config) => {
    let dbUrl = `mongodb://${config.hostname}:${config.port}`;

    mongoose.Promise = Promise;

    mongoose.connection.on('connected', () => {
        console.log('Connection to database established')
    })

    mongoose.connection.on('reconnected', () => {
        console.log('Connection to database reestablished')
    })

    mongoose.connection.on('disconnected', () => {
        console.log('Disconnected from database. Reconnecting in 10 seconds...');
        setTimeout(() => {connect(dbUrl, config)}, 10000);
    })

    mongoose.connection.on('close', () => {
        console.log('Connection to database closed')
    })

    mongoose.connection.on('error', (error) => {
        console.log('ERROR: ' + error)
    })

    await connect(dbUrl, config).catch(error => console.error(error));
};

module.exports = setUpDbConnection;