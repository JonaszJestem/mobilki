const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: {type: String, required: true},
    password: {type: String, required: true},
    email: {type: String, required: false},
    friends: {type: Array, required: true},
    picture: {type: String, required: false},
    token: {type: String, required: false}
});

module.exports = mongoose.model('User', userSchema);
