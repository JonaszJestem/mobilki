const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema({
    name: {type: String, required: true},
    description: {type: String, required: false},
    date: {type: Date, required: false, default: Date.now()},
    location: {type: String, required: true},
    participants: {type: Array, required: false},
    host: {type: String, required: true},
    image: {type: String, required: false},
    public: {type: Boolean, required: true, default: true}
});

module.exports = mongoose.model('Event', eventSchema);
